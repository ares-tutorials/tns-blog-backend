import fs from 'fs';
import admin from 'firebase-admin';
import express from "express";
import { db, connectToDb } from './db.js';
import path from 'path';

const credentials = JSON.parse(
    fs.readFileSync('./credentials.json')
);

// [HINT]: INITIALIZE FIREBASE-ADMIN PACKAGE
admin.initializeApp({
    credential: admin.credential.cert(credentials),
});

const app = express();

// [STRUCTURE]: MIDDLEWARES
app.use(express.json()); // [HINT]:[ADDITIONAL SETUP STEPS] In order for req.body property to work in react, we have to add a middleware. Above endpoints. Whenever there's a request that seems to have a JSON payload, parse it automatically and make it avaialble.
app.use(async (req, res, next) => {
    const { authtoken } = req.headers;
    if (authtoken) {
        try {
            req.user = await admin.auth().verifyIdToken(authtoken); // [HINT]: Verify the token, and provide corresponding user id ( uid ) associated with it.
        } catch (e) {
            return res.sendStatus(400);
        }
    }
    req.user = req.user || {};
    next(); //[HINT]: Moves forward to routes requested

});

app.get('/api/articles/:name', async (req, res) => {
    const { name } = req.params;
    const { uid } = req.user;

    const article = await db.collection('articles').findOne({ name });
    if (article) {
        const upvoteIds = article.upvoteIds || []; // [PURPOSE]: this object in database will hold which uid's have ever upvoted the article. If its already upvoted, further upvote won't be allowed.
        article.canUpvote = uid && !upvoteIds.includes(uid);
        res.json(article);
    } else {
        res.sendStatus(404).send('Article Not Found');
    }
});

// [PURPOSE]: Following MW will check if user is logged-in or not. If not it won't allow the action to be performed.

app.use((req, res, next) => {
    if (req.user) {
        next();
    } else {
        res.sendStatus(401);
    }
});

app.put('/api/articles/:name/upvote', async (req, res) => {
    const { name } = req.params;
    const { uid } = req.user;
    console.log(`name ${name}`)
    const article = await db.collection('articles').findOne({ name });
    if (article) {
        const upvoteIds = article.upvoteIds || []; // [PURPOSE]: Check previous.
        const canUpvote = uid && !upvoteIds.includes(uid);
        
        if (canUpvote) {
            await db.collection('articles').updateOne({ name }, {
                $inc: { upvotes: 1 },
                $push: { upvoteIds: uid }
            });
        }

        const updatedArticle = await db.collection('articles').findOne({ name });
        res.json(updatedArticle);
    } else {
        res.send('That article doesn\'t exist');
    }
});

app.post('/api/articles/:name/comments', async (req, res) => {
    const { name } = req.params;
    const { text } = req.body;
    const { email } = req.user; //[HINT]: Firebase user obj has email the property.

    await db.collection('articles').updateOne({ name }, {
        $push: {
            comments: {
                email, text
            }
        }
    })
    const article = await db.collection('articles').findOne({ name });

    if (article) {
        res.json(article);
    } else {
        res.send('That article does\'nt exist')
    }
});

connectToDb(() => {
    console.log('Successfully Connected to database');
    app.listen(8000, () => {
        console.log('Server is listening on port 8000');
    });
});
